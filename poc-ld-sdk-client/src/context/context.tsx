import { FeatureFlag } from "@rentspree/feature-flag"
import { createContext } from "react";

export interface AppContextInterface {
  instance: FeatureFlag | null
  migratedInstance: FeatureFlag | null
  userId: string
}

export const MyContext = createContext<AppContextInterface>({ instance: null, migratedInstance: null, userId: "" });