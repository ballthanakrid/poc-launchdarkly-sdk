import { MyContext } from '../../context/context';
import Content from '../content/content';
import './main.css';

const Main = () => {
  return (
    <div className="main-body">
      <MyContext.Consumer>
        {value => <Content instance={value.instance} migratedInstance={value.migratedInstance} userId={value.userId} ></Content>}
      </MyContext.Consumer>
    </div>
  );
}

export default Main;
