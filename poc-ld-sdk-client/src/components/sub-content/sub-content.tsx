import { FeatureFlag } from '@rentspree/feature-flag/build/src';
import { useEffect, useState } from 'react';
import './sub-content.css';

interface FeatureFlagData {
  status: boolean;
  variation: string;
}

const SubContent = ({ header, featureKeys, instance, userId }: {header: string; featureKeys: string[]; instance: FeatureFlag; userId: string }) => {
  const [flagDataList, setFlagDataList] = useState<FeatureFlagData[]>([])
  const [isReady, setIsReady] = useState<boolean>(false)
  const setFlagInitializeState = async () => {
    await instance.onReady()
    setIsReady(true)
  }
  const setData = async () => {
    const flagData: FeatureFlagData[] = featureKeys.map((featureKey: string) => {
      let variation = instance.getVariation(featureKey, userId, {});
      return { status: instance.isFeatureEnabled(featureKey, userId) as boolean, variation: variation as string }
    })
    setFlagDataList(flagData)
  }
  useEffect(() => {
    setFlagInitializeState()
    if(instance && isReady && flagDataList.length === 0) {
      setData()
    }
    
  }, [instance, isReady, flagDataList])
  return (
      <section>
      <h3> {header }</h3>
      {
        flagDataList.map((flagData: FeatureFlagData, index: number) => {
          return (
            <div key={index.toString()}>
              <p>
                key: {featureKeys[index]}
              </p>
              <p>
              status: {flagData.status.toString()}
              </p>
              <p>
              variation: {flagData.variation}
              </p>
            </div>
            
          )
        })
      }
      </section>
  );
}

export default SubContent;
