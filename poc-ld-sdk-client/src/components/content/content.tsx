import { AppContextInterface } from '../../context/context';
import SubContent from '../sub-content/sub-content';
import './content.css';

const Content = ({ instance, migratedInstance, userId }: AppContextInterface) => {
  const ldFeatureKeys = ['test-flag', 'test-multivariate', 'test-flag-2']
  const optFeatureKeys = ['agent_branding_1_0', 'renters_insurance_1_3', 'renters_insurance', 'renters_insurance_1_3_test']


  return (
    <div className="content">
      {
        migratedInstance && <SubContent header='LD Feature Flags' featureKeys={ldFeatureKeys} instance={migratedInstance} userId={userId}></SubContent>
      }
      {
        instance && <SubContent header='Optimizely Feature Flags' featureKeys={optFeatureKeys} instance={instance} userId={userId}></SubContent>
      }
    </div>

    
  );
}

export default Content;
