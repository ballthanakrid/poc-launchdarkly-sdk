import * as LDClient from "launchdarkly-js-client-sdk";
import './App.css';
import { useContext, useEffect, useState } from 'react';
import Main from "./components/main/main";
import { AppContextInterface, MyContext } from "./context/context";
import flag, { IGeneralConfig, InstanceType } from "@rentspree/feature-flag"



function App() {
  const user = {
    id: "test-id",
    email: "ball@rentspree.com",
    integration: "none",
    key: "aa0ceb"
  }
  const clientId = "61653c64ede67b24aa0aff33"
  const ldConfig: IGeneralConfig = {
    credentialKey: clientId,
    userContext: {id: user.id, attributes: {...user} },
    options: { evaluationReasons: true }
  }
  const ldInstance = flag.createInstance(InstanceType.LD_CLIENT_SIDE, ldConfig)
  const optimizelySdkKey = "STdb9jPGb34NE2iLak9zyV"
  const optimizelyInstance = flag.createInstance(InstanceType.OPTIMIZELY, {
    sdkKey: optimizelySdkKey,
    datafileOptions: {
      autoUpdate: false,
      urlTemplate: `https://cdn.optimizely.com/datafiles/STdb9jPGb34NE2iLak9zyV.json`,
    },
    credentialKey: "",
  })
  

  return (
    <div>
<MyContext.Provider value={{ instance: optimizelyInstance, migratedInstance: ldInstance, userId: user.id }}>
      <div className="App">
      
      <Main></Main>
    </div>
    </MyContext.Provider>

    <MyContext.Consumer>
        {value => <TestComponent instance={value.instance} migratedInstance={value.migratedInstance} userId={value.userId} ></TestComponent>}
      </MyContext.Consumer>
    </div>
    
    
  );
}

const TestComponent = ( context: AppContextInterface) => {
  const value = useContext(MyContext)
  useEffect(() => {
    console.log("From use context",value)

  }, [value])
  useEffect(() => {
    console.log("Consumer",context)

  }, [context])
  return <div>

  </div>
}

export default App;
