import express, { Application, Request, Response } from "express";
import { getOptimizelyClient, getLdClient, initClient } from "./feature-flag";

const app: Application = express();
const port = 3010;

// Body parsing Middleware
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.post(
    "/feature-flags/ld",
    async (req: Request, res: Response): Promise<Response> => {
      const featureKeys = ['rental_document']
      const user = {
        // id: "test-id",
        // email: "ball@rentspree.com",
        // integration: "none",
        ...req.body,
      }
      const client = getLdClient()
      const flagDetails = await Promise.all(featureKeys.map(async(key: string, index: number) => {
        const isEnabled: boolean | undefined = await client?.isFeatureEnabled(key, user.id, user);
        const variation: string | undefined = await client?.getVariation(key, user.id, user)
        return {
          key: featureKeys[index],
          isEnabled,
          variation
        };
      }))
      return res.status(200).send({
        flagDetails
      });
    }
);

app.get(
  "/feature-flags/optimizely",
  async (req: Request, res: Response): Promise<Response> => {
    const featureKeys = ['agent_branding_1_0', 'renters_insurance_1_3_test', 'renters_insurance']      
    const user = {
      id: "test-id",
      email: "ball@rentspree.com",
      integration: "none",
      key: "aa0ceb"
    }
    const client = getOptimizelyClient()

    const flagDetails = featureKeys.map((key: string, index: number) => {
      const isEnabled: boolean | undefined = client?.isFeatureEnabled(key, user.id) as boolean;
      const variation: string | undefined = client?.getVariation(key, user.id, user) as string;
      return {
        key: featureKeys[index],
        isEnabled,
        variation
      };
    })
    return res.status(200).send({
      flagDetails
    });
  }
);

try {
    app.listen(port, async ():  Promise<void> => {
        await initClient()
        console.log(`Connected successfully on port ${port}`);
    });
} catch (error: any ) {
    console.error(`Error occured: ${error.message}`);
}