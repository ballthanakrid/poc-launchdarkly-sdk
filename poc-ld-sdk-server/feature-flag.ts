import flag, { FeatureFlag, IGeneralConfig, InstanceType } from "@rentspree/feature-flag"
let optimizelyClient: FeatureFlag | null = null
let ldClient: FeatureFlag | null = null
export const getOptimizelyClient = () => {
  return optimizelyClient
}

export const getLdClient = () => {
  return ldClient
}

export const initClient = async () => {
  const sdkKey = "sdk-bd7e0a60-b9bd-43d1-a974-64efaed3daf5"
  const ldConfig: IGeneralConfig = {
    credentialKey: sdkKey,
  }
  ldClient = flag.createInstance(InstanceType.LD_SERVER_SIDE, ldConfig)
  const optimizelySdkKey = "STdb9jPGb34NE2iLak9zyV"
  optimizelyClient = flag.createInstance(InstanceType.OPTIMIZELY, {
    sdkKey: optimizelySdkKey,
    datafileOptions: {
      autoUpdate: false,
      urlTemplate: `https://cdn.optimizely.com/datafiles/STdb9jPGb34NE2iLak9zyV.json`,
    },
    credentialKey: "",
  })
  await ldClient.onReady()
  await optimizelyClient.onReady()
}